#ifndef __BUYER_HH__
#define __BUYER_HH__

class Buyer
{
        private :
                char *bodyType ;
                char *wheelType ;
        protected :
        public :
                Buyer(){};
                virtual ~Buyer(){};
                int getData() ;
                void setData();
                void display();
};
class RetailBuyer : public Buyer
{
        private :
        public :
                RetailBuyer(){};
                virtual ~RetailBuyer(){};
                bool requestAProduct();
                int getData() ;
                void setData();
                void display();
};
class EnterpriseBuyer : public Buyer
{
        private :
        public :
                EnterpriseBuyer(){};
                virtual ~EnterpriseBuyer(){};
                bool requestClassicProduct();
                int getData() ;
                void setData();
                void display();
};
#endif
