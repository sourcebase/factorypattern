#ifndef __FACTORY_HH__
#define __FACTORY_HH__

class ProductFactory
{
        private :
        public :
                ProductFactory(){};
                virtual ~ProductFactory(){};
                int getData() ;
                void setData();
                void display();
};
class BodyFactory : public ProductFactory
{
        private :
        public :
                BodyFactory();
                virtual ~BodyFactory(){};
                int getData() ;
                void setData();
                void display();
};
class WheelFactory : public ProductFactory
{
        private :
        public :
                WheelFactory();
                virtual ~WheelFactory(){};
                int getData() ;
                void setData();
                void display();
};
#endif